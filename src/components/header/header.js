import React from "react";
import moment from "moment";

export default class Header extends React.Component {

    countUsers = (messages) => {
        let idList = [];
        for (let {userId} of messages) {
            if (!idList.includes(userId)) {
                idList.push(userId);
            }
        }

        return idList.length;
    }


    render() {
        const { messages } = this.props;

        const usersCount = messages ? this.countUsers(messages): null;
        const messagesCount = messages?.length;
        const lastMessageDate = moment(messages[messagesCount - 1]?.createdAt).format('DD.MM.YYYY HH:mm');

        return (
            <header className='header navbar navbar-dark bg-light card'>
                <div className='container-fluid'>
                    <div className='header-title'>
                        My chat
                    </div>
                    <div>
                        <span  className='header-users-count'>
                        {usersCount}
                        </span> users
                    </div>
                    <div>
                        <span  className='header-messages-count'>
                            {messagesCount}
                        </span> messages
                    </div>
                    <div>
                        last message at
                        <span className='header-last-message-date'>
                            {lastMessageDate}
                        </span>
                    </div>
                </div>
            </header>
        );
    }
}