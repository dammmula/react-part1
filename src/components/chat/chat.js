import React from "react";
import Preloader from "../preloader/preloader";
import Header from "../header/header";
import MessageList from "../message-list/message-list";
import MessageInput from "../message-input/message-input";

import './chat.css';
import moment from "moment";

export default class Chat extends React.Component {

    messageIds = 0;

    state = {
        messages: null,
        messageInput: '',
        userId: 0,
        messageToEditId: null
    }

    sortMessages = (messages) => {
        messages.sort(({createdAt: a}, {createdAt: b}) => {
            return moment(a) - moment(b);
        });
        return messages;
    }

    componentDidMount = async () => {
        const res = await fetch(this.props.url);

        if (!res.ok) {
            throw new Error(`Could not fetch ${this.props.url}, received ${res.status}`);
        }

        const data = await res.json();
        this.setState({ messages: this.sortMessages(data) });
    }

    createMessage = (text) => {
        return {
            text,
            createdAt: moment().format(),
            editedAt: '',
            userId: this.state.userId,
            id: ++this.messageIds
        }
    }

    onMessageInput = (event) => {
        this.setState({
            messageInput: event.target.value
        });
    }

    onMessageSend = () => {
        let messages;

        if (this.state.messageToEditId) {
            messages = this.state.messages.map((message) => {
                if (message.id === this.state.messageToEditId) {
                    message.text = this.state.messageInput;
                    return message;
                }
                return message;
            })
        } else {
            const newMessage = this.createMessage(this.state.messageInput);
            messages = [...this.state.messages, newMessage];
        }

        this.setState({
            messages,
            messageInput: '',
            messageToEditId: null
        });
    }

    onEditMessage = (id) => {
        const message = this.state.messages
            .find((item) => item.id === id);
        this.setState({
            messageInput: message.text,
            messageToEditId: id
        })
    }

    onDeleteMessage = (id) => {
        const messages = this.state.messages
            .filter((message) => message.id !== id);
        this.setState({ messages });
    }

    render() {
        const { messages } = this.state;

        if (!messages) {
            return <Preloader />;
        }

        return (
            <div className='chat container'>
                <Header messages={messages}/>
                <MessageList
                    onEditMessage={this.onEditMessage}
                    onDeleteMessage={this.onDeleteMessage}
                    messages={messages}
                    userId={this.state.userId} />
                <MessageInput
                    text={this.state.messageInput}
                    onMessageInput={this.onMessageInput}
                    onMessageSend={this.onMessageSend} />
            </div>
        );
    }
}